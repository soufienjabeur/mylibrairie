import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MyLibrairie';
  constructor() {
    const firebaseConfig = {
      apiKey: 'AIzaSyDK2X-4Ahj8bIHTn4sUiMiSTo3L_VoWW3k',
      authDomain: 'mylibrairie-b630c.firebaseapp.com',
      databaseURL: 'https://mylibrairie-b630c.firebaseio.com',
      projectId: 'mylibrairie-b630c',
      storageBucket: 'mylibrairie-b630c.appspot.com',
      messagingSenderId: '212209135636',
    };
    firebase.initializeApp(firebaseConfig);
  }
}
