import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/modules/book.model';
import { ActivatedRoute, Router } from '@angular/router';
import { LivresService } from 'src/app/services/livres.service';

@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.scss']
})
export class SingleBookComponent implements OnInit {

  book: Book;
  constructor(private route: ActivatedRoute, private booksService: LivresService, private router: Router) { }

  ngOnInit() {
    this.book = new Book('', '');
    const id = this.route.snapshot.params['id'];
    this.booksService.getSingleBook(+id).then(
      (book: Book) => {
        this.book = book;
      }
    );
  }

  onBack() {
    this.router.navigate(['/books']);
  }
}
