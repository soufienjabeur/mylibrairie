import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LivresService } from 'src/app/services/livres.service';
import { Router } from '@angular/router';
import { Book } from 'src/app/modules/book.model';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.scss']
})
export class BookFormComponent implements OnInit {

  bookform: FormGroup;

  constructor(private formBuilder: FormBuilder, private livreService: LivresService, private router: Router) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.bookform = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      synopsis: ''
    });

  }

  onSaveBook() {
    const title = this.bookform.get('title').value;
    const author = this.bookform.get('author').value;
    const synopsis = this.bookform.get('synopsis').value;
    const newBook = new Book(title, author);
    newBook.synopsis = synopsis;
    this.livreService.createNewBook(newBook);
    this.router.navigate(['/books']);

  }
}
