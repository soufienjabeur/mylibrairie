# MyLibrairie

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Installing Bootstrap

Run `npm install bootstap --save`

Run `npm install --save bootstap@x.x.x` for specific version

## Installing RxJS librairie

Run `npm install rxjs-compat --save`

## Installing FireBase

Run `npm install firebase --save`

## Workflow

Add the services created to the providers's array of AppModule

@angular/forms: Implements a set of directives and providers to communicate with native DOM elements when building forms to capture user input.

Add the modules FormsModule, ReactiveFormsModule to the imports's array.

To communicate with the backend services, we use HTTP by adding HttpClientModule to the import's array of AppModule.

Adding routing even without guard, so we can to access to all the sections of the app.

Adding the module's folder and create the class Book

Preparing the HeaderComponent with a navigation menu and adding routerLink

Adding the HeaderComponent to the AppComponent.

Adding firebase's configuration providing by FireBase's site after creating a project in it to AppComponent's constructer.

Adding three methods to AuthService:

- createNewUser(): to create new user and it takes as args an email and password of the user and return a promise.
- signInUser(): to sign in a user and it takes as args email and passwor of the user and return a promise
- signOutUser(): to deconnect a user.

Working in the SignupComponent by adding two methods

- initForm(): create a reactive form with to required field : email and password.

- onSubmit(): to create new user, which call the function onCreateUser() in the AuthService service. If succes, user will be forword to books page, else user get an error message.

Designing the SignupComponent's form.

Working in the SigninComponent by adding two methods

- initForm(): create a reactive form with to required field : email and password.

- onSubmit(): to create new user, which call the function onCreateUser() in the AuthService service. If succes, user will be forword to books page, else user get an error message.

Designing the SigninComponent's form.

Overwriting the headerComponent

Adding signout() methode to the headerComponent

Overwriting the design of headerComponent to deal with sign up of a new user, sign in of existance user and sign out of connecting user

Adding canActive() method to the authGuardService so only user, who is authentified, can access to the app

Working in the bookService, where we will implement 6 methods

- emitBooks(): transmit books to a specific node of the dataBase.

- saveBooks(): save the list of books in a specific node of the dataBase.

- getBooks(): get the list of books fom a specific node of the dataBase.

- getSingleBook(): get a single book fom a specific node of the dataBase.

- createNewBook(): creat new book and add it to the list of the books

- removeBook(): remove a specific book.

Working in the bookListComponent, where will implement 5 methods

- ngOnInit(): subscribe in the subject that we have created in bookService and run his first transmission.

- onNewBook(): show the list of books

- onDeleteBook(): remove a single book form the list of books

- onViewBook(): show a single book

- ngOnDestroy(): unscribe from the subject that we have created in boolService

Designing the bookListComponent page

Working in the singleBookComponent, where will implement 2 methods
- ngOnInit(): get a book using the id, and pin up it on next page

- onBack(): go back to the list of books

Designing the singleBookComponent page.

Working in the bookFormComponent, where will implement 2 methods
 
 - initForm(): initialize the form of create of new book
 
 - onSaveBook():  
